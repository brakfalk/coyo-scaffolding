(function () {
  'use strict';

  var testhelper = require('./../testhelper.js');

  function Navigation() {
    var api = this;

    var navbar = $('.main-navigation .coyo-navbar');

    api.home = navbar.$('a[ui-sref="main.landing-page"]');
    api.pages = navbar.$('a[ui-sref="main.page"]');
    api.workspaces = navbar.$('a[ui-sref="main.workspace"]');
    api.events = navbar.$('a[ui-sref="main.event"]');
    api.searchIcon = navbar.$('.nav-search .search-icon');
    api.notification = {
      open: function () {
        navbar.$('.notifications-dialog-parent .zmdi-notifications-none').click();
      },
      isUnseen: function () {
        testhelper.hasClass($('.notifications-dialog-parent a span'), 'notifications-unseen').then(function (bool) {
          return bool;
        });
      },
      discussion: element.all(by.repeater('category in ::$ctrl.categories')).get(0),
      activity: element.all(by.repeater('category in ::$ctrl.categories')).get(1),
      notifications: element.all(by.repeater('notification in $ctrl.data[category].items')),
      time: function (index) {
        return api.notification.notifications.get(index).$('.time');
      }
    };
    api.search = {
      searchInput: function (term) {
        navbar.element(by.model('$root.search.term')).sendKeys(term);
      }
    };
    api.colleagues = navbar.$('a[ui-sref="main.colleagues"]');
    api.profile = navbar.$('a[ui-sref="main.profile({userId: $ctrl.user.slug})"]');
    api.profileMenu = {
      open: function () {
        api.profileMenu.toggle.click();
      },
      toggle: navbar.$('.nav-item-profile .zmdi-caret-down'),
      admin: navbar.$('a[ui-sref="admin"]'),
      editView: navbar.$('[translate="NAVIGATION.EDIT.VIEW"]'),
      account: navbar.$('a[ui-sref="main.account"]'),
      notifications: navbar.$('a[ui-sref="main.account-notifications"]'),
      fileLibrary: navbar.$('a[coyo-file-library-trigger]'),
      logout: navbar.$('a[ng-click="$ctrl.logout()"]'),
      toggleModeratorMode: function () {
        // toggling the moderator bar causes the page to reload which sometimes breaks protractor, so a hard wait for now
        browser.ignoreSynchronization = true;
        navbar.$('a[ng-click="$ctrl.toggleModeratorMode()"]').click();
        browser.sleep(1000);
        browser.ignoreSynchronization = false;
        testhelper.disableAnimations();
      }
    };
    api.admin = {
      exit: $('a[ui-sref="main"] span[translate="ADMIN.MENU.EXIT"]'),
      userDirectories: $('a[ui-sref="admin.user-directories.list"]'),
      authProviders: $('a[ui-sref="admin.authentication-providers.list"]')
    };
    api.logout = function () {
      api.profileMenu.open();
      api.profileMenu.logout.click();
    };
    api.editView = function () {
      api.profileMenu.open();
      api.profileMenu.editView.click();
    };
    api.viewEditOptions = {
      saveButton: $('.view-edit-options-bar a[ng-click="$ctrl.save()"]'),
      cancelButton: $('.view-edit-options-bar a[ng-click="$ctrl.cancel()"]')
    };
    api.getHome = function () {
      browser.get('/home');
    };
    api.getSearch = function () {
      browser.get('/search');
    };
  }

  module.exports = Navigation;

})();
