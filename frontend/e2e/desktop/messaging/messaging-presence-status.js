(function () {
  'use strict';

  var login = require('./../../login.page.js');
  var sidebar = require('./messaging-sidebar.page.js');
  var NavigationPage = require('../navigation.page.js');
  var testhelper = require('../../testhelper.js');

  var username, password, navigation;

  describe('presence status', function () {
    beforeAll(function () {
      navigation = new NavigationPage();
      login.loginDefaultUser();
      var key = Math.floor(Math.random() * 1000000);
      username = 'max.mustermann.' + key + '@mindsmash.com';
      password = 'Secret123';
      testhelper.createUser('Max', 'Mustermann' + key, username, password, ['User']);
      login.logout();
      login.login(username, password);
    });

    beforeEach(function () {
      navigation.getHome();
    });

    it('should see presence status as online', function () {
      expect(sidebar.onlineStatus.isPresent()).toBe(true);
    });

    it('should change presence status', function () {
      // toggle form
      sidebar.onlineStatus.click();
      expect(sidebar.presenceStatusView.parent.isPresent()).toBe(true);

      // click AWAY status
      sidebar.presenceStatusView.awayState.click();

      browser.sleep(50); // wait a little bit for web-socket

      // close form
      sidebar.presenceStatusView.close.click();

      // then
      expect(sidebar.onlineStatus.isPresent()).toBe(false);
      expect(sidebar.awayStatus.isPresent()).toBe(true);
    });
  });

})();
