(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var Navigation = require('./../../navigation.page');
  var moderatorBar = require('./moderator-bar.fragment');
  var testhelper = require('./../../../testhelper');
  var PageList = require('./../../pages/page-list.page');

  describe('moderator mode', function () {

    var email, pageName;

    beforeEach(function () {
      email = 'TestyMcTest' + Date.now() + '@moderatorTest.com';
      pageName = 'TestModeratorModePage' + Date.now();

      login.loginDefaultAdminUser();
      testhelper.createUser('Testy', 'McTest', email, 'Demo123', ['Admin']);
    });

    it('should make private pages visible', function () {
      var navbar = new Navigation();
      var pages = new PageList();

      testhelper.createPage(pageName, 'PRIVATE').then(function () {
        login.logout();
        login.login(email, 'Demo123');
        testhelper.cancelTour();
        navbar.profileMenu.open();
        navbar.profileMenu.toggleModeratorMode();

        expect(moderatorBar.bar.isPresent()).toBeTruthy();
        navbar.pages.click();
        testhelper.cancelTour();

        pages.searchInput.clear().then(function () {
          pages.search(pageName).then(function (array) {
            expect(array.length).toBe(1);
          });
        });

        // clear input otherwise input text is present forever
        pages.searchInput.clear().then(function () {
          navbar.profileMenu.open();
          navbar.profileMenu.toggleModeratorMode();
          browser.wait(moderatorBar.bar.isPresent().then(function (value) { return !value; }), 10000);
        });
      });

    });

    afterEach(function () {
      // logout current user and login as an admin user
      login.logout();
      login.loginDefaultAdminUser();
      // delete created users and pages
      testhelper.deletePages();
      testhelper.deleteUsers();
    });
  });
})();
