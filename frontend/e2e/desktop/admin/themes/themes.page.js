(function () {
  'use strict';

  module.exports = {
    get: function () {
      browser.get('/admin/themes');
      require('../../../testhelper.js').disableAnimations();
    },
    colors: {
      primary: $('#theme_setting_color-primary'),
      mainBackground: $('#theme_setting_color-background-main')
    },
    saveButton: $('.panel-footer .btn-primary')
  };

})();
