(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var testhelper = require('../../../testhelper.js');
  var list = require('./admin.role-list.page.js');
  var details = require('./admin.role-details.page.js');
  var components = require('./../../../components.page.js');

  describe('role administration', function () {

    beforeAll(function () {
      login.loginDefaultUser();
    });

    beforeEach(function () {
      list.get();
    });

    afterEach(function () {
      // set default role to role = User
      testhelper.setDefaultRole('User');
    });

    it('should sort the role list by name', function () {

      // descending
      list.table.headers.name.click();
      list.table.rows.names().then(function (values) {
        expect(values).toEqual(values.slice().sort(compareIgnoreCase).reverse());
      });

      // ascending
      list.table.headers.name.click();
      list.table.rows.names().then(function (values) {
        expect(values).toEqual(values.slice().sort(compareIgnoreCase));
      });
    });

    it('should create, update and delete role with CREATE_PAGE permission', function () {
      var suffix = Math.floor(Math.random() * 1000000); // for unique test data
      var name = 'testrole' + suffix;

      // create role
      list.createButton.click();
      expect(details.isSaveButtonDisabled()).toBe(true);

      details.name.sendKeys(name);
      expect(details.isSaveButtonDisabled()).toBe(false);
      details.defaultRole.click();
      details.permissionGroup('PAGES').click();
      details.permission('CREATE_PAGE').click();
      details.saveButton.click();

      // validate the created role
      verifyRole(name);

      // update the role name
      details.name.sendKeys('-updated');
      details.saveButton.click();

      // validate the updated role
      verifyRole(name + '-updated');

      // delete role
      details.cancelButton.click();
      openOptions().deleteOption.click();
      components.modals.confirm.confirmButton.click();
      expect(list.table.rows.count()).toBe(0);
    });

    it('should create, update and delete role with a group and CREATE_PAGE permission', function () {
      var suffix = Math.floor(Math.random() * 1000000); // for unique test data
      var name = 'testrole' + suffix;
      var groupName = 'Admins';
      var groupNameAlt = 'Users';

      // create role
      list.createButton.click();
      expect(details.isSaveButtonDisabled()).toBe(true);

      details.name.sendKeys(name);
      expect(details.isSaveButtonDisabled()).toBe(false);
      details.defaultRole.click();
      details.permissionGroup('PAGES').click();
      details.permission('CREATE_PAGE').click();
      details.groups.openDropdown();
      details.groups.selectOption(groupName);
      details.saveButton.click();

      // validate the created role
      verifyRole(name, [groupName]);

      // update the role name
      details.groups.openDropdown();
      details.groups.selectOption(groupNameAlt);
      details.saveButton.click();

      // validate the updated role
      verifyRole(name, [groupName, groupNameAlt]);

      // delete role
      details.cancelButton.click();
      openOptions().deleteOption.click();
      components.modals.confirm.confirmButton.click();
      expect(list.table.rows.count()).toBe(0);
    });

    function openOptions() {
      var options = list.table.rows.get(0).options();
      options.open();
      return options;
    }

    function verifyRole(expectedName, expectedGroups) {
      list.nameFilter.clear();
      list.nameFilter.sendKeys(expectedName);
      expect(list.table.rows.count()).toBe(1);
      openOptions().editOption.click();

      expect(details.name.getAttribute('value')).toBe(expectedName);
      expect(details.isDefaultRole()).toBe(true);

      expect(details.permission('CREATE_PAGE').isChecked()).toBe(true);

      expectedGroups && expectedGroups.forEach(function (groupName) {
        expect(details.groups.selectedOptions()).toContain(groupName);
      });
    }

    function compareIgnoreCase(a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    }
  });

})();
