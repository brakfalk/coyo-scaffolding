(function () {
  'use strict';

  var repeater = 'row in vm.page.content track by row.id';

  module.exports = {
    get: function () {
      browser.get('/admin/user-management/groups');
      require('../../../testhelper.js').disableAnimations();
    },
    nameFilter: element(by.model('$ctrl.searchTerm')),
    groupsTotal: $('counter span.hidden-xs'),
    createButton: $('.fb-actions-inline a[ui-sref="admin.user-management.groups.create"]'),
    table: {
      headers: {
        name: element(by.css('th[property="displayName"]'))
      },
      rows: {
        get: function (index) {
          var row = $$('tr[ng-repeat="' + repeater + '"]').get(index);
          return {
            row: row,
            name: row.$$('td').get(0),
            directory: row.$$('td').get(1),
            roles: row.$$('td').get(2),
            users: row.$$('td').get(3),
            options: function () {
              var el = row.$$('td').get(4);
              return {
                open: function () {
                  browser.actions().mouseMove(el).perform();
                  el.$('.dropdown-toggle').click();
                },
                deleteOption: el.$('span[translate="ADMIN.USER_MGMT.GROUPS.OPTIONS.DELETE.MENU"]'),
                editOption: el.$('span[translate="ADMIN.USER_MGMT.GROUPS.OPTIONS.EDIT.MENU"]')
              };
            }
          };
        },
        names: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(0).getText();
          });
        },
        count: function () {
          return element.all(by.repeater('' + repeater + '')).count();
        }
      },
      pagination: {
        links: element.all(by.css('.pagination-page')),
        first: $('.pagination-first'),
        prev: $('.pagination-prev'),
        next: $('.pagination-next'),
        last: $('.pagination-last')
      }
    }
  };

})();
