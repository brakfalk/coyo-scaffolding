(function () {
  'use strict';

  module.exports = {
    get: function () {
      browser.get('/admin');
      require('../../testhelper.js').disableAnimations();
    },
    navigation: {
      multiLanguageLink: $('#admin-sidebar-multiLanguage')
    }
  };

})();
