(function () {
  'use strict';

  function SelectSender(selectSenderElement) {
    var api = this;
    api.element = selectSenderElement;
    api.entries = api.element.all(by.repeater('option in $group.items'));
    api.getEntry = function (index) {
      var entryElement = api.entries.get(index);
      return {
        element: entryElement,
        senderName: entryElement.$('.select-sender-display-name')
      };
    };
  }

  SelectSender.getElement = function (context) {
    return context.$('coyo-select-sender');
  };

  SelectSender.create = function (context) {
    var element = SelectSender.getElement(context);
    return new SelectSender(element);
  };

  module.exports = SelectSender;

})();
