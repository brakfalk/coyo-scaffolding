(function () {
  'use strict';

  function Colleagues() {
    var api = this;
    var testhelper = require('../../testhelper.js');

    api.filter = {
      all: $('.fb-col-filter-transclude').$('li[text-key="MODULE.COLLEAGUES.FILTER.ALL"] a')
    };

    api.paginationList1 = element.all(by.repeater('page in pages track by $index'));

    api.list = {
      paginationList: element.all(by.repeater('page in pages track by $index')),
      activePage: function () {
        var element;
        return api.list.paginationList.each(function (elem) {
          testhelper.hasClass(elem, 'active').then(function (bool) {
            if (bool) {
              element = elem;
            }
          });
        }).then(function () {
          return element;
        });
      },
      nextPage: $('a[ng-click="selectPage(page + 1, $event)"]'),
      userByName: function (name) {
        return element(by.cssContainingText('.user-card-title', name));
      }
    };

  }

  module.exports = Colleagues;

})();
