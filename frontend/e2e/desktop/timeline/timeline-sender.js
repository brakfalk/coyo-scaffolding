(function () {
  'use strict';

  var login = require('../../login.page.js');
  var testhelper = require('../../testhelper.js');
  var SenderTimeline = require('./timeline-sender.page.js');
  var Navigation = require('../navigation.page.js');
  var PageList = require('../pages/page-list.page.js');
  var PageDetails = require('../pages/page-details.page.js');

  describe('sender timeline', function () {
    var senderTimeline, navigation, pageList, pageDetails;

    beforeAll(function () {
      senderTimeline = new SenderTimeline();
      navigation = new Navigation();
      pageList = new PageList();
      pageDetails = new PageDetails();
      login.loginDefaultUser();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterAll(function () {
      testhelper.deleteTimelineEntry();
      testhelper.deleteTimelines();
      testhelper.deletePages();
    });

    it('load more on home timeline', function () {
      var pageName = 'SenderTimelineMyFirstTestPage' + Math.floor(Math.random() * 1000000);
      var PAGE_SIZE = 8;
      // create page and timeline entries
      createPageAndTimelineItems(pageName, PAGE_SIZE);

      // refresh browser to mark timeline items as 'seen'
      browser.refresh();
      checkNumOfTimelineItems(PAGE_SIZE);
    });

    it('load more on page timeline', function () {
      var pageName = 'SenderTimelineMySecondTestPage' + Math.floor(Math.random() * 1000000);
      var PAGE_SIZE = 8;
      // create page and timeline entries
      createPageAndTimelineItems(pageName, PAGE_SIZE);

      navigation.pages.click();
      testhelper.cancelTour();

      pageList.search(pageName).get(0).$('h4[ng-click="ctrl.openPage(page)"]').click();
      pageDetails.options.addApp.click();
      pageDetails.addApp.timeline.click();
      pageDetails.addApp.save.click();
      checkNumOfTimelineItems(PAGE_SIZE);

    });

    it('comment on item on page timeline', function () {
      var pageName = 'SenderTimelineMyThirdTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          var timelineItemMessage1 = 'This is a new timelineentry';
          testhelper.createTimelineEntry(timelineItemMessage1, pageId).then(function () {

            var timelineItemMessage2 = 'This is a new timelineentry 123';
            testhelper.createTimelineEntry(timelineItemMessage2, pageId).then(function () {

              var timelineItem1 = senderTimeline.getTimelineItem(0);
              var commentText1 = 'This is a text for a comment 1';
              timelineItem1.comments.addComment(commentText1).then(function (comment1) {
                var commentText2 = 'This is a text for a comment 2';
                timelineItem1.comments.addComment(commentText2).then(function (comment2) {
                  expect(timelineItem1.message.getText()).toBe(timelineItemMessage2);
                  expect(comment1.message.getText()).toBe(commentText1);
                  expect(comment2.message.getText()).toBe(commentText2);
                });
              });

              var timelineItem2 = senderTimeline.getTimelineItem(1);
              var commentText3 = 'This is a text for a comment 123 1';
              timelineItem2.comments.addComment(commentText3).then(function (comment3) {
                var commentText4 = 'This is a text for a comment 123 2';
                timelineItem2.comments.addComment(commentText4).then(function (comment4) {
                  expect(timelineItem2.message.getText()).toBe(timelineItemMessage1);
                  expect(comment3.message.getText()).toBe(commentText3);
                  expect(comment4.message.getText()).toBe(commentText4);
                });
              });
            });
          });
        });
      });
    });

    it('comment on item on page timeline as another sender', function () {
      var pageName = 'SenderTimelineCommentAsSenderTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          var timelineItemMessage = 'This is a new timeline entry';
          testhelper.createTimelineEntry(timelineItemMessage, pageId).then(function () {
            var commentText = 'This is a text for a comment';
            var timelineItem = senderTimeline.getTimelineItem(0);
            timelineItem.selectSender.element.click();
            var senderEntryToSelect = timelineItem.selectSender.getEntry(1),
                senderName = senderEntryToSelect.senderName.getText();
            senderEntryToSelect.element.click();
            timelineItem.comments.addComment(commentText).then(function (comment) {
              expect(comment.message.getText()).toBe(commentText);
              expect(comment.author.getText()).toBe(senderName);
            });
          });
        });
      });
    });

    it('like timelineItem', function () {
      var pageName = 'SenderTimelineMyThirdTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();
          testhelper.createTimelineEntry('An Item to be liked', pageId).then(function () {
            var timelineItem = senderTimeline.getTimelineItem(0);
            timelineItem.likeBtn.element.click();
            timelineItem.likeBtn.text.getText().then(function (text) {
              expect(text).toBe('–You like this');
            });
          });
        });
      });
    });

    it('like timelineItem as another sender', function () {
      var pageName = 'SenderTimelineLikeAsSenderTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();
          testhelper.createTimelineEntry('An Item to be liked', pageId).then(function () {
            var timelineItem = senderTimeline.getTimelineItem(0);
            timelineItem.selectSender.element.click();
            var senderEntryToSelect = timelineItem.selectSender.getEntry(1);
            senderEntryToSelect.senderName.getText().then(function (senderName) {
              senderEntryToSelect.element.click();
              timelineItem.likeBtn.element.click();
              expect(timelineItem.likeBtn.text.getText()).toBe('–You like this');
              timelineItem.selectSender.element.click();
              timelineItem.selectSender.getEntry(0).element.click();
              expect(timelineItem.likeBtn.text.getText()).toBe('–' + senderName + ' likes this');
            });
          });
        });
      });
    });

    it('like comment on timeline item as user and another sender', function () {
      var pageName = 'SenderTimelineLikeCommentAsSenderTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          var timelineItemMessage = 'This is a new timeline entry';
          testhelper.createTimelineEntry(timelineItemMessage, pageId).then(function () {
            var timelineItem = senderTimeline.getTimelineItem(0);
            timelineItem.comments.addComment('Test comment').then(function (comment) {
              expect(comment.likeBtn.count.getText()).toBe('0');
              comment.likeBtn.element.click();
              timelineItem.selectSender.element.click();
              var senderEntryToSelect = timelineItem.selectSender.getEntry(1),
                  userName = timelineItem.selectSender.getEntry(0).senderName.getText(),
                  senderName = senderEntryToSelect.senderName.getText();
              senderEntryToSelect.element.click();
              expect(comment.likeBtn.count.getText()).toBe('1');
              comment.likeBtn.element.click();
              expect(comment.likeBtn.count.getText()).toBe('2');
              browser.actions().mouseMove(comment.likeBtn.count).perform();
              expect(comment.likeBtn.tooltipCondensed.isDisplayed()).toBe(true);
              expect(comment.likeBtn.tooltipCondensed.getText()).toContain(userName);
              expect(comment.likeBtn.tooltipCondensed.getText()).toContain(senderName);
            });
          });
        });
      });
    });

    it('should show sticky timeline item and mark it as read', function () {
      var pageName = 'SenderTimelineStickyTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          var timelineItemMessage = 'This is a sticky timeline item.';
          createStickyTimelineItem(timelineItemMessage, pageId).then(function () {
            var timelineItem = senderTimeline.getTimelineItem(0);
            expect(timelineItem.stickyRibbon.isDisplayed()).toBe(true);
            expect(timelineItem.stickyReadRibbon.isDisplayed()).toBe(true);
            timelineItem.markAsRead();
            expect(timelineItem.stickyRibbon.isPresent()).toBe(false);
            expect(timelineItem.stickyReadRibbon.isPresent()).toBe(false);
          });
        });
      });
    });

    it('should remove stickiness of timeline item', function () {
      var pageName = 'SenderTimelineRemoveStickinessTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          var timelineItemMessage = 'This is a sticky timeline entry.';
          testhelper.createTimelineEntry(timelineItemMessage, pageId, {stickyExpiry: 10000}).then(function () {
            var timelineItem = senderTimeline.getTimelineItem(0);
            expect(timelineItem.stickyRibbon.isDisplayed()).toBe(true);
            expect(timelineItem.stickyReadRibbon.isDisplayed()).toBe(true);
            timelineItem.menu.openAndSelectOption('Unsticky');
            timelineItem.unstickyModal.confirmButton.click();
            expect(timelineItem.stickyRibbon.isPresent()).toBe(false);
            expect(timelineItem.stickyReadRibbon.isPresent()).toBe(false);
          });
        });
      });
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('embed video', function () {
      var pageName = 'SenderTimelineTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();
          senderTimeline.form.messageField
              .sendKeys('An Item with a video link: https://www.youtube.com/watch?v=MSNf1C6kaP8 .');
          senderTimeline.form.submitBtn.click();
          var timelineItem = senderTimeline.getTimelineItem(0);
          expect(timelineItem.embeddedVideo.isDisplayed()).toBe(true);
          expect(timelineItem.embeddedVideo.$('iframe')
              .getAttribute('src')).toBe('https://www.youtube.com/embed/MSNf1C6kaP8');
        });
      });

    }).pend('Test is skipped because video preview is not always working. Bug is reported');

    function createPageAndTimelineItems(pageName, pagesize) {
      testhelper.createPage(pageName).then(function (pageId) {
        for (var i = 0; i < pagesize + pagesize; i++) {
          testhelper.createTimelineEntry('new entry for the timeline ' + i, pageId);
        }
      });
    }

    function checkNumOfTimelineItems(pagesize) {
      // check timeline items before loadMore()
      senderTimeline.timelineItems.count().then(function (num) {
        expect(num).toBe(pagesize);
      });

      // load more for timeline
      senderTimeline.loadMore.click();
      senderTimeline.timelineItems.count().then(function (num) {
        expect(num).toBe(pagesize + pagesize);
      });
    }

    function createStickyTimelineItem(message, pageId) {
      return testhelper.createTimelineEntry(message, pageId, {stickyExpiry: 10000});
    }

  });
})();
