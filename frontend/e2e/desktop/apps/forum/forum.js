(function () {
  'use strict';

  var WorkspaceDetail = require('./../../workspaces/workspace-details.page');
  var components = require('./../../../components.page');
  var ForumHelper = require('./forum-helper');
  var ForumPage = require('./forum.page');
  var login = require('./../../../login.page');
  var testHelper = require('./../../../testhelper');
  var path = require('path');

  // some tests can not be started separately
  describe('forum app', function () {
    var workspaceDetail, workspaceName, forumHelper, forumPage;

    beforeAll(function () {
      workspaceDetail = new WorkspaceDetail();
      forumHelper = new ForumHelper();
      forumPage = new ForumPage();
      var key = Math.floor(Math.random() * 1000000);

      login.loginDefaultUser();
      // create workspace and navigate to it
      workspaceName = 'forum-app-test-workspace-' + key;
      testHelper.createWorkspace(workspaceName);
    });

    beforeEach(function () {
      browser.get('/workspaces/' + workspaceName);
    });

    afterAll(function () {
      testHelper.deleteWorkspace();
    });

    it('create forum app', function () {
      forumHelper.createEmptyForumApp();
    });

    it('create forum thread without attachment', function () {
      forumHelper.createForumThread('Forum thread test 1', 'Forum thread test 1 text');
      expect(forumPage.thread.view.title.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.text.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.addAnswer.title.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.addAnswer.avatar.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.addAnswer.editor.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.addAnswer.saveButton.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.addAnswer.saveButton.getAttribute('disabled')).toBe('true');
      forumPage.thread.view.toOverview.click();
      forumPage.threadList.threadLinks.count().then(function (entries) {
        expect(entries).toEqual(1);
      });
    });

    it('create forum thread with attachment', function () {
      var attachmentPath = path.resolve(__dirname, 'attachment.txt');
      forumHelper.createForumThread('Forum thread test 2', 'Forum thread test 2 text', attachmentPath);
    });

    it('create a thread answer', function () {
      forumPage.threadList.threadLinks.first().click();
      forumPage.thread.view.addAnswer.editor.clear();
      forumPage.thread.view.addAnswer.editor.sendKeys('answer test comment...');
      forumPage.thread.view.addAnswer.saveButton.click();
      forumPage.thread.view.answers.count().then(function (entries) {
        expect(entries).toEqual(1);
      });
      expect(forumPage.thread.view.answer.info.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.answer.author.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.answer.creator.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.answer.time.isPresent()).toBeTruthy();
      expect(forumPage.thread.view.answer.text.isPresent()).toBeTruthy();
    });

    it('delete a thread answer', function () {
      forumPage.threadList.threadLinks.first().click();
      forumHelper.hoverElement(forumPage.thread.view.answer.contextMenu.hover);
      forumPage.thread.view.answer.contextMenu.toggle.click();
      forumPage.thread.view.answer.contextMenu.remove.click();
      components.modals.confirm.confirmButton.click();
      forumPage.thread.view.answers.count().then(function (entries) {
        expect(entries).toEqual(1);
      });
      expect(forumPage.thread.view.answer.deleted.isPresent()).toBeTruthy();
    });

    it('view forum thread list', function () {
      forumPage.threadList.threadLinks.count().then(function (entries) {
        expect(entries).toEqual(2);
      });
      var thread1 = forumPage.threadList.threads.get(0);
      expect(thread1.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
      expect(thread1.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();
      expect(thread1.element(forumPage.threadList.entry.title).isPresent()).toBeTruthy();
      expect(thread1.element(forumPage.threadList.entry.created.author).isPresent()).toBeTruthy();

      var thread2 = forumPage.threadList.threads.get(1);
      expect(thread2.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
      expect(thread2.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();
      expect(thread2.element(forumPage.threadList.entry.title).isPresent()).toBeTruthy();
      expect(thread2.element(forumPage.threadList.entry.created.author).isPresent()).toBeTruthy();
    });

    describe('forum thread context menu', function () {

      it('create a thread for context menu tests', function () {
        forumHelper.createForumThread('Forum thread test 3', 'Forum thread test 3 text');
        expect(forumPage.thread.view.status.container.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.status.open.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.status.pinned.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.status.closed.isPresent()).toBeFalsy();
      });

      it('pin a thread', function () {
        forumPage.threadList.threadLinks.first().click();
        forumHelper.hoverElement(forumPage.thread.view.contextMenu.hover);
        expect(forumPage.thread.view.contextMenu.toggle.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.toggle.click();
        expect(forumPage.thread.view.contextMenu.pin.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.unpin.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.close.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.reopen.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.pin.click();
        expect(forumPage.thread.view.status.open.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.status.pinned.isPresent()).toBeTruthy();
      });

      it('unpin a thread', function () {
        forumPage.threadList.threadLinks.first().click();
        forumHelper.hoverElement(forumPage.thread.view.contextMenu.hover);
        forumPage.thread.view.contextMenu.toggle.click();
        expect(forumPage.thread.view.contextMenu.pin.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.unpin.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.close.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.reopen.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.unpin.click();
        expect(forumPage.thread.view.status.open.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.status.pinned.isPresent()).toBeFalsy();
      });

      it('close a thread', function () {
        forumPage.threadList.threadLinks.first().click();
        forumHelper.hoverElement(forumPage.thread.view.contextMenu.hover);
        forumPage.thread.view.contextMenu.toggle.click();
        expect(forumPage.thread.view.contextMenu.pin.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.unpin.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.close.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.reopen.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.close.click();
        components.modals.confirm.confirmButton.click();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.closedMessage.isPresent()).toBeTruthy();
      });

      it('reopen a thread', function () {
        forumPage.threadList.threadLinks.last().click();
        forumHelper.hoverElement(forumPage.thread.view.contextMenu.hover);
        forumPage.thread.view.contextMenu.toggle.click();
        expect(forumPage.thread.view.contextMenu.pin.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.unpin.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.close.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.reopen.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.reopen.click();
        expect(forumPage.thread.view.status.closed.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.status.open.isPresent()).toBeTruthy();
      });

      it('delete a thread', function () {
        forumPage.threadList.threadLinks.first().click();
        forumHelper.hoverElement(forumPage.thread.view.contextMenu.hover);
        forumPage.thread.view.contextMenu.toggle.click();
        expect(forumPage.thread.view.contextMenu.pin.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.unpin.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.close.isPresent()).toBeTruthy();
        expect(forumPage.thread.view.contextMenu.reopen.isPresent()).toBeFalsy();
        expect(forumPage.thread.view.contextMenu.remove.isPresent()).toBeTruthy();
        forumPage.thread.view.contextMenu.remove.click();
        components.modals.confirm.confirmButton.click();
        forumPage.threadList.threadLinks.count().then(function (entries) {
          expect(entries).toEqual(2);
        });
      });

    });

    describe('forum thread list element context menu', function () {

      it('pin a thread', function () {
        var thread = forumPage.threadList.threads.last();
        forumHelper.hoverElement(thread.element(forumPage.threadList.contextMenu.hover));
        thread.element(forumPage.threadList.contextMenu.toggle).click();
        thread.element(forumPage.threadList.contextMenu.pin).click();

        thread = forumPage.threadList.threads.first();
        expect(thread.element(forumPage.threadList.entry.icon.pinned.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.pinned.foreground).isPresent()).toBeTruthy();

        thread = forumPage.threadList.threads.last();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();
      });

      it('unpin a thread', function () {
        var thread = forumPage.threadList.threads.first();
        forumHelper.hoverElement(thread.element(forumPage.threadList.contextMenu.hover));
        thread.element(forumPage.threadList.contextMenu.toggle).click();
        thread.element(forumPage.threadList.contextMenu.unpin).click();

        thread = forumPage.threadList.threads.first();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();

        thread = forumPage.threadList.threads.last();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();
      });

      it('close a thread', function () {
        var thread = forumPage.threadList.threads.first();
        forumHelper.hoverElement(thread.element(forumPage.threadList.contextMenu.hover));
        thread.element(forumPage.threadList.contextMenu.toggle).click();
        thread.element(forumPage.threadList.contextMenu.close).click();
        components.modals.confirm.confirmButton.click();

        thread = forumPage.threadList.threads.first();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();

        thread = forumPage.threadList.threads.last();
        expect(thread.element(forumPage.threadList.entry.icon.closed.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.closed.foreground).isPresent()).toBeTruthy();
      });

      it('reopen a thread', function () {
        var thread = forumPage.threadList.threads.last();
        forumHelper.hoverElement(thread.element(forumPage.threadList.contextMenu.hover));
        thread.element(forumPage.threadList.contextMenu.toggle).click();
        thread.element(forumPage.threadList.contextMenu.reopen).click();

        thread = forumPage.threadList.threads.first();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();

        thread = forumPage.threadList.threads.last();
        expect(thread.element(forumPage.threadList.entry.icon.open.background).isPresent()).toBeTruthy();
        expect(thread.element(forumPage.threadList.entry.icon.open.foreground).isPresent()).toBeTruthy();
      });

      it('delete a thread', function () {
        var thread = forumPage.threadList.threads.last();
        forumHelper.hoverElement(thread.element(forumPage.threadList.contextMenu.hover));
        thread.element(forumPage.threadList.contextMenu.toggle).click();
        thread.element(forumPage.threadList.contextMenu.remove).click();
        components.modals.confirm.confirmButton.click();
        forumPage.threadList.threads.count().then(function (entries) {
          expect(entries).toEqual(1);
        });
      });

    });

    it('delete forum app', function () {
      var navApps = workspaceDetail.navigation.apps;
      var elem = navApps.last();
      forumHelper.hoverElement(elem);
      workspaceDetail.navigation.openAppSettings(elem);
      // delete button
      components.modals.confirm.deleteButton.click();
      // confirm delete button
      components.modals.confirm.deleteButton.click();
      // check if deleted
      expect(navApps.count()).toBe(0);
    });

  });
})();
