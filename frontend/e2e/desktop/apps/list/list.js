(function () {
  'use strict';

  var WorkspaceDetail = require('./../../workspaces/workspace-details.page.js');
  var ListApp = require('./list.page.js');
  var ListEntry = require('./list-entry.page.js');
  var ListHelper = require('./list-helper');
  var component = require('./../../../components.page');
  var Profile = require('./../../profile/profile.page.js');

  var login = require('./../../../login.page.js');
  var testhelper = require('./../../../testhelper.js');

  // specs of this test can not be started separately
  describe('list app', function () {
    var workspaceDetail, listApp, listEntry, workspaceName, workspaceSlug, listHelper, profile;

    var userFieldName = 'user-field';
    var optionFieldName = 'options';
    var optionFieldoption1 = 'options One';
    var optionFieldoption2 = 'options Two';
    var optionFieldoption3 = 'options Three';
    var textFieldName = 'Some text';
    var checkboxFieldName = 'Some checkbox name';

    beforeAll(function () {
      workspaceDetail = new WorkspaceDetail();
      listApp = new ListApp();
      listEntry = new ListEntry();
      listHelper = new ListHelper();
      profile = new Profile();
      var key = Math.floor(Math.random() * 1000000);

      login.loginDefaultUser();
      // create workspace and navigate to it
      workspaceName = 'MyNewWorkspace_Listapp_' + key;
      workspaceSlug = 'mynewworkspace-listapp-' + key;
      testhelper.createWorkspace(workspaceName);
      browser.get('/workspaces/' + workspaceSlug);
      listHelper.createEmptyListApp();
    });

    afterAll(function () {
      testhelper.deleteWorkspace();
    });

    beforeEach(function () {
      browser.get('/workspaces/' + workspaceSlug);
    });

    it('should create empty list app', function () {
      expect(listApp.listAppTable.emptyAppDefaultText.isPresent()).toBeTruthy();
      expect(listApp.listAppTable.newFieldBtn.isPresent()).toBeTruthy();
    });

    it('should be possible to add user field', function () {
      listHelper.createUserField(userFieldName);

      expect(listApp.configureFields.items.count()).toBe(1);
      expect(listApp.configureFields.fieldType(0).getText()).toBe('User');
      expect(listApp.configureFields.fieldName(0).getText()).toBe(userFieldName);
    });

    it('should be possible to add options field', function () {
      listHelper.createOptionsField(optionFieldName, optionFieldoption1, optionFieldoption2, optionFieldoption3);

      expect(listApp.configureFields.items.count()).toBe(2);
      expect(listApp.configureFields.fieldType(1).getText()).toBe('Options');
      expect(listApp.configureFields.fieldName(1).getText()).toBe(optionFieldName);
    });

    it('should be possible to add text field', function () {
      listHelper.createTextField(textFieldName);

      expect(listApp.configureFields.items.count()).toBe(3);
      expect(listApp.configureFields.fieldType(2).getText()).toBe('Text');
      expect(listApp.configureFields.fieldName(2).getText()).toBe(textFieldName);
    });

    it('should be possible to add checkbox field', function () {
      listHelper.createCheckboxField(checkboxFieldName);

      expect(listApp.configureFields.items.count()).toBe(4);
      expect(listApp.configureFields.fieldType(3).getText()).toBe('Checkbox');
      expect(listApp.configureFields.fieldName(3).getText()).toBe(checkboxFieldName);
    });

    it('should show add entry button after adding any field', function () {
      listHelper.createTextField(textFieldName);
      listApp.configureFields.backBtn.click();
      // check empty list text after creating some fields
      expect(listApp.listAppTable.emptyAppDefaultText.isPresent()).toBeTruthy();
      expect(listApp.listAppTable.newEntryBtn.isPresent()).toBeTruthy();
    });

    it('should add an entry to a list', function () {
      // add entry to list
      listApp.listAppTable.newEntryBtn.click();

      listEntry.chooseUser.choose();
      var userName = 'Nancy Fork';
      listEntry.chooseUser.selectUserName(userName);
      listEntry.chooseUser.selectUserBtn.click();
      component.modals.confirm.confirmButton.click();
      expect(listApp.listAppTable.entries.count()).toBe(1);
    });

    it('should use inline edit checkbox', function () {
      var firstEntry = listApp.listAppTable.entries.first();
      var checkboxIcon = firstEntry.$('coyo-list-inline-edit-checkbox i');
      expect(checkboxIcon.isDisplayed()).toBe(true);
      expect(checkboxIcon.evaluate('checkboxCtrl.fieldValue.value')).toBe(false);
      checkboxIcon.click();
      browser.refresh();
      expect(checkboxIcon.evaluate('checkboxCtrl.fieldValue.value')).toBe(true);
      checkboxIcon.click();
    });

    it('should view history of entry', function () {
      listApp.listAppTable.viewDetailEntry(0);

      expect(listApp.entryDetailView.historyLabel.isPresent()).toBeTruthy();

      listApp.entryDetailView.historyBtn.click();
      listApp.entryDetailView.createdByUser.click();
      expect(profile.title.getText()).toBe('Ian Bold');

      browser.navigate().back();
      expect(listApp.entryDetailView.entryDetailsList.isPresent()).toBeTruthy();
    });


    it('search for user in list app', function () {

      expect($$('.dynamic-app-table-row').count()).toBe(1);

      listApp.listAppTable.search('Ian');

      expect(listApp.listAppTable.emptyAppDefaultText.isPresent()).toBeTruthy();

      listApp.listAppTable.search('Na');

      expect($$('.dynamic-app-table-row').count()).toBe(1);
    });

    it('show form when view of items is not allowed', function () {
      var elem = workspaceDetail.navigation.apps.last();
      browser.actions().mouseMove(elem).perform();
      workspaceDetail.navigation.openAppSettings(elem);


      listApp.setting.advanced.click();

      listApp.setting.seeEntryGroup.none.click();

      component.modals.confirm.confirmButton.click();

      login.login('rl', 'demo');

      browser.get('/workspaces/' + workspaceSlug);

      workspaceDetail.navigation.apps.last().click();

      expect(listEntry.noReadPermissionsLabel.isPresent()).toBeTruthy().then(function () {
        // restore the initial test situation
        login.logout();
        login.loginDefaultUser();
      });
    });

    it('delete list app', function () {
      var navApps = workspaceDetail.navigation.apps;
      var elem = navApps.last();
      browser.actions().mouseMove(elem).perform();

      workspaceDetail.navigation.openAppSettings(elem);
      // delete button
      component.modals.confirm.deleteButton.click();
      // confirm delete button
      component.modals.confirm.deleteButton.click();
      // check if deleted
      expect(navApps.count()).toBe(0);
    });

  });
})();
