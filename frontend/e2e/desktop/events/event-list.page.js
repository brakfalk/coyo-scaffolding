(function () {
  'use strict';

  function EventList() {
    var api = this;

    api.newButton = $('.actions-vertical a[ui-sref="main.event.create"]');
    api.searchInput = element(by.model('$ctrl.searchTerm'));
    api.search = function (eventName) {
      element(by.model('$ctrl.searchTerm')).sendKeys(eventName);
      return element.all(by.css('.event-card'));
    };

    api.list = {
      eventList: element.all(by.repeater('event in $ctrl.currentPage.content'))
    };

    api.events = element.all(by.repeater('event in $ctrl.currentPage.content'));

    api.navigateTo = function (elem) {
      elem.$('a[ng-click="$ctrl.open($event)"]').click();
    };
  }

  module.exports = EventList;

})();
