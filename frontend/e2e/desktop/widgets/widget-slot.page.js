(function () {
  'use strict';

  var Widget = require('./widget.page');

  function WidgetSlot(name) {
    var api = this;
    var container = $('.widget-slot-' + name);

    api.addButton = container.$('.widget-slot-options .zmdi-plus');
    api.editButton = container.$('.widget-slot-options .zmdi-edit');
    api.editWidgetButton = container.$('[ng-click="$ctrl.editWidget(widget, widgetScope)"]');
    api.removeWidgetButton = container.$('[ng-click="$ctrl.removeWidget(widget.model)"]');
    api.allWidgets = container.$$('.widget');
    api.getWidget = function (index) {
      return new Widget(api.allWidgets.get(index));
    };
    api.widgetChooser = new WidgetChooser();
  }

  function WidgetChooser() {
    var api = this;
    api.selectByName = function (name) {
      api.searchField.sendKeys(name);
      api.getWidgetByName(name).click();
    };
    api.saveButton = $('.modal-footer .btn-primary');
    api.backButton = $('.modal-footer .btn-default');
    api.searchField = $('.widget-filter-container input');
    api.getWidgetByName = function (name) {
      return $('#all-widgets').element(by.xpath('.//h5[.="' + name + '"]'));
    };
  }

  module.exports = WidgetSlot;

})();

