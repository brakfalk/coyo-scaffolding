(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var SingleFileWidget = require('./single-file-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  // eslint-disable-next-line jasmine/no-disabled-tests
  xdescribe('singlefile widget', function () {
    var widgetSlot, widgetChooser, widget, singleFileWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('content');
      navigation = new Navigation();
      navigation.editView();
      widgetChooser = widgetSlot.widgetChooser;
      singleFileWidget = new SingleFileWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('should be created', function () {
      // count actual widgets
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // add new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Single File');
      // TODO file selection / upload
      widgetSlot.widgetChooser.saveButton.click();

      // count
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // remove widget
      navigation.editView();
      singleFileWidget.hover();
      singleFileWidget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();

      // count
      expect(widgetSlot.allWidgets.count()).toBe(0);
    }).pend('test needs existing file attachments in demo data or file upload to work');
  });
})();
