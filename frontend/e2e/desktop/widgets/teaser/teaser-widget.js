(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var TeaserWidget = require('./teaser-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('teaser widget', function () {
    var widgetSlot, widgetChooser, widget, teaserWidget, navigation;
    var relativeUrl = '/workspaces/daily-tasks';
    var absoluteUrl = 'https://www.coyoapp.com/de/home';

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('content');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      teaserWidget = new TeaserWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created and configurable', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Teaser');

      // configure
      teaserWidget.openSettings(0);
      teaserWidget.setUrl(relativeUrl);
      teaserWidget.saveSettings();

      teaserWidget.openSettings(1);
      teaserWidget.setUrl(absoluteUrl);
      teaserWidget.saveSettings();

      // save
      widgetChooser.saveButton.click();
      navigation.viewEditOptions.saveButton.click();

      // count
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // navigate
      teaserWidget.clickSlide(0);
      expect(browser.getCurrentUrl()).toMatch(browser.baseUrl + relativeUrl);

      browser.navigate().back();
      teaserWidget.clickSlide(1);
      expect(browser.getCurrentUrl()).toMatch(absoluteUrl);

      browser.navigate().back();


      // remove widget
      navigation.editView();
      teaserWidget.hover();

      teaserWidget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
