(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var UpcomingEventsWidget = require('./upcoming-events-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('upcoming events widget', function () {
    var widgetSlot, widgetChooser, widget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
    });

    afterEach(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Upcoming Events');

      //settings
      var settings = new UpcomingEventsWidget().settings;
      settings.numberOfDisplayedEvents.clear();
      settings.numberOfDisplayedEvents.sendKeys('3');

      //save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // remove widget
      widget.hover();
      widget.removeButton.click().then(function () {
        components.modals.confirm.confirmButton.click();
        navigation.viewEditOptions.saveButton.click();
        expect(widgetSlot.allWidgets.count()).toBe(0);
        expect(widgetSlot.addButton.isPresent()).toBe(false);
      });

      expect(widgetSlot.allWidgets.count()).toBe(0);
      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });
})();
