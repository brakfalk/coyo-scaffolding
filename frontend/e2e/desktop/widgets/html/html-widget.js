(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var HtmlWidget = require('./html-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('html widget', function () {
    var widgetSlot, widgetChooser, widget, htmlWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      var key = Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage('html-widget-testpage-' + key);
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      htmlWidget = new HtmlWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created and use html5 tags', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('HTML');

      // setup
      var html = '<details>\n'
          + '  <summary>Copyright 2017</summary>\n'
          + '  <p> - by Coyo GmbH. All Rights Reserved.</p>\n'
          + '  <p>All content on this web site are the property of the company Coyo GmbH.</p>\n'
          + '</details>';
      htmlWidget.settings.input.sendKeys(html);

      // save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // check html5 elements
      expect(htmlWidget.content.detailsTag.isPresent()).toBeTruthy();
      expect(htmlWidget.content.summaryTag.isPresent()).toBeTruthy();

      // remove widget
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);
      expect(widgetSlot.addButton.isPresent()).toBeFalsy();
    });
  });

})();
