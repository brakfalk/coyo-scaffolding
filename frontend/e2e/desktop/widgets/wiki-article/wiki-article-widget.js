(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var WikiArticleWidget = require('./wiki-article-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('wiki article widget', function () {
    var widgetSlot, widgetChooser, widget, wikiArticleWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      wikiArticleWidget = new WikiArticleWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Wiki Article');

      wikiArticleWidget.settings.wikiArticle.openDropdown();
      wikiArticleWidget.settings.wikiArticle.selectOption('What are Pages?');

      // save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);
      expect(wikiArticleWidget.renderedWidget.articleSender.getText()).toBe('About Coyo');
      expect(wikiArticleWidget.renderedWidget.articleTitle.getText()).toBe('What are Pages?');

      // remove widget
      widget.hover();
      widget.removeButton.click().then(function () {
        components.modals.confirm.confirmButton.click();
        navigation.viewEditOptions.saveButton.click();
        expect(widgetSlot.allWidgets.count()).toBe(0);

        expect(widgetSlot.addButton.isPresent()).toBe(false);
      });
    });
  });

})();
