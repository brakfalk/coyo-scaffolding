(function () {
  'use strict';

  var extend = require('util')._extend;
  var UserChooser = require('../../user-chooser/user-chooser.page.js');
  var Checkbox = require('../../../checkbox.page');

  function UserProfileWidget(widget) {
    var api = extend(this, widget);
    var userprofileClass = '.user-profile-widget';

    api.settings = {
      showInfo: new Checkbox($('form[name="settingsForm"]').element(by.model('model.settings._showInfo'))),
      userChooser: new UserChooser('model.settings')
    };

    api.renderedUserProfile = {
      name: $(userprofileClass + ' h4 a'),
      jobTitle: $(userprofileClass + ' p'),
      email: $(userprofileClass).element(by.binding('$ctrl.user.email')),
      phone: $(userprofileClass).element(by.binding('$ctrl.user.properties.phone')),
      department: $(userprofileClass).element(by.binding('$ctrl.user.properties.department'))
    };
  }

  module.exports = UserProfileWidget;

})();
