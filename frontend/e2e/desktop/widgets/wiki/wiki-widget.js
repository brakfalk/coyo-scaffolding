(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var WikiWidget = require('./wiki-widget.page.js');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('wiki widget', function () {
    var widgetSlot, widgetChooser, widget, wikiWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      wikiWidget = new WikiWidget(widget);
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Latest Wiki Articles');

      // settings
      wikiWidget.settings.articleCount.clear();
      wikiWidget.settings.articleCount.sendKeys('3');

      wikiWidget.settings.wikiApp.openDropdown();
      wikiWidget.settings.wikiApp.selectOption('FAQs');

      // save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);
      expect(wikiWidget.allArticles.count()).toBe(3);

      // remove widget
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
