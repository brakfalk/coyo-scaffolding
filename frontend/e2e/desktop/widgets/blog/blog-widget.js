(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var BlogWidget = require('./blog-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');

  describe('blog widget', function () {
    var widgetSlot, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetSlot.widgetChooser.selectByName('Latest Blog Articles');

      // settings
      var settings = new BlogWidget().settings;
      settings.sourceSelection.selected.click();
      settings.sources.openDropdown();
      settings.sources.selectOption('Information');
      settings.articleCount.clear();
      settings.articleCount.sendKeys('2');

      // save
      widgetSlot.widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // test
      var view = new BlogWidget().view;
      expect(view.blogs.count()).toBe(2);

      // remove widget
      var blogWidget = new BlogWidget(widgetSlot.getWidget(0));
      blogWidget.hover();

      var rmBtn = blogWidget.removeButton;

      browser.wait(function () {
        return rmBtn.isPresent().then(function (present) {
          if (present) {
            // click remove widget button as long it is present
            browser.actions().mouseMove(rmBtn).click().perform();
          }
          return present;
        });
      }, 2000);

      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
