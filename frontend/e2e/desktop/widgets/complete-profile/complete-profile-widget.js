(function () {
  'use strict';

  var login = require('../../../login.page.js');
  var WidgetSlot = require('../widget-slot.page.js');
  var CompleteProfileWidget = require('./complete-profile-widget.page.js');
  var testhelper = require('../../../testhelper');
  // var components = require('../../components.page');
  var ProfilePage = require('../../profile/profile.page.js');
  var NavigationPage = require('../../navigation.page.js');
  var PageList = require('../../pages/page-list.page.js');
  // var path = require('path');

  describe('complete-profile widget', function () {

    var widgetSlot, widgetChooser, profile, widget, navigation, completeProfileWidget, pageList, usernameAdmin,
        passwordAdmin, pageName;

    beforeAll(function () {
      navigation = new NavigationPage();
      profile = new ProfilePage();
      pageList = new PageList();

      login.loginDefaultUser();

      // create two admin user one to create test and one to complete profile
      var key = Math.floor(Math.random() * 1000000);
      var username = 'max.mustermann.' + key + '@mindsmash.com';
      var password = 'Secret123';
      testhelper.createUser('Max', 'Mustermann' + key, username, password, ['Admin', 'User']);

      var keyAdmin = Math.floor(Math.random() * 1000000);
      usernameAdmin = 'maxi.admin.' + key + '@mindsmash.com';
      passwordAdmin = 'Secret123';
      testhelper.createUser('Maxi', 'Admin' + keyAdmin, usernameAdmin, passwordAdmin, ['Admin', 'User']);

      login.logout();
      login.login(usernameAdmin, passwordAdmin);

      pageName = 'completeProfileTestpage' + Math.floor(Math.random() * 1000000);
      testhelper.createAndOpenPage(pageName);

      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      completeProfileWidget = new CompleteProfileWidget(widget);
      navigation.editView();
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Complete Profile');
      navigation.viewEditOptions.saveButton.click();

      navigation.logout();
      login.login(username, password);
      browser.get('/pages/' + pageName);
      testhelper.cancelTour();
    });

    afterAll(function () {
      login.logout();
      login.login(usernameAdmin, passwordAdmin);
      testhelper.deletePages();
      testhelper.deleteUsers();
    });

    it('should be created', function () {
      expect(completeProfileWidget.renderedWidget.isNotChecked('Fill in your profile details')).toBe(true);
      expect(completeProfileWidget.renderedWidget.isNotChecked('Upload a profile picture')).toBe(true);
      expect(completeProfileWidget.renderedWidget.isNotChecked('Upload a cover picture')).toBe(true);
      expect(completeProfileWidget.renderedWidget.isNotChecked('Follow a user')).toBe(true);
      expect(completeProfileWidget.renderedWidget.isNotChecked('Follow a page')).toBe(true);
      expect(completeProfileWidget.renderedWidget.isNotChecked('Post something on your wall')).toBe(true);

      // edit profile fields
      completeProfileWidget.renderedWidget.links.get(0).click();
      expect(browser.getCurrentUrl()).toMatch(/\/info/);

      testhelper.cancelTour();

      browser.actions()
          .mouseMove(profile.work.form.getWebElement(), {x: 0, y: 0})
          .perform();

      profile.work.editButton.click();
      profile.work.jobTitleInput.sendKeys('CEO');
      profile.work.companyInput.sendKeys('mustermannGmbH');
      profile.work.departmentInput.sendKeys('Management');
      profile.work.locationInput.sendKeys('Hamburg');
      profile.work.submitButton.click();
      browser.navigate().back();

      // upload a profile picture (deactivated, not working on bamboo)
      /*
      completeProfileWidget.renderedWidget.links.get(1).click();
      expect(browser.getCurrentUrl()).toMatch(/\/activity/);

      profile.wall.addProfilePic.click();

      var fileToUpload = './complete-profile-widget-test-image.png';
      var absolutePath = path.resolve(__dirname, fileToUpload); //eslint-disable-line

      profile.wall.fileUpload.sendKeys(absolutePath);
      profile.wall.submitFileUpload.click();
      browser.navigate().back();
      */

      // upload a cover picture (deactivated, not working on bamboo)
      /*
      completeProfileWidget.renderedWidget.links.get(2).click();
      expect(browser.getCurrentUrl()).toMatch(/\/activity/);

      profile.wall.addCoverPic.click();
      profile.wall.fileUpload.sendKeys(absolutePath);
      profile.wall.submitFileUpload.click();
      browser.navigate().back();
      */

      // follow a user
      completeProfileWidget.renderedWidget.links.get(3).click();
      expect(browser.getCurrentUrl()).toMatch(/\/colleagues/);
      testhelper.cancelTour();

      completeProfileWidget.filterColleaguesList.click();
      testhelper.cancelTour();
      completeProfileWidget.followUser.click();
      browser.navigate().back();

      // follow a page
      navigation.pages.click();
      testhelper.cancelTour();
      //clear input search field to make sure it is empty if any previous test did not clear search field
      pageList.searchInput.clear();
      pageList.filterAll.click();
      var pagesAsList = pageList.list.pageList;
      expect(pagesAsList.count()).toBeGreaterThan(3);

      pageList.list.followPage(pageName).click();

      expect(pageList.list.subscribeButtonSuccess(pageName).isPresent()).toBe(true);

      browser.navigate().back();

      // post something
      completeProfileWidget.renderedWidget.links.get(5).click();
      expect(browser.getCurrentUrl()).toMatch(/\/profile/);
      testhelper.cancelTour();

      profile.wall.postOnWallTextarea.sendKeys(
          'Hey all! I finally finished my profile thanks to the complete your profile widget.');
      profile.wall.postOnWallSubmit.click();
      browser.navigate().back();

      // Checks if the widget won't be displayed anymore after completing profile information - will probably fail because protractor isn't able to load cover image constantly
      // expect(completeProfileWidget.renderedWidget.elemHasClass($('[ng-repeat="widget in $ctrl.widgets | filter:{deleted:false}"]'), 'ng-hide')).toBe(true);
    });

  });
})();
