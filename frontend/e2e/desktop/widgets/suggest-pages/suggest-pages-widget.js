(function () {
  'use strict';

  var login = require('../../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var SuggestPageWidget = require('./suggest-pages-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../testhelper');
  var PageDetails = require('../../pages/page-details.page');

  describe('suggest page widget', function () {
    var widgetSlot, navigation, name, page;

    beforeAll(function () {
      name = 'tobeselected' + Math.floor(Math.random() * 1000000);
      login.loginDefaultUser();
      testhelper.createAndOpenPage(name);
      page = new PageDetails();
      // unsubscribe current page
      page.imageHeader.subscribe.click();
      navigation = new Navigation();
      navigation.editView();

      widgetSlot = new WidgetSlot('page-sidebar-bottom');
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should be created and display picked page', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetSlot.widgetChooser.selectByName('Suggested Pages');

      // settings
      var settings = new SuggestPageWidget().settings;

      settings.pageSelector.click();
      settings.pageSelector.sendKeys(name);
      settings.pageApp.selectOption(name);

      // save
      widgetSlot.widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      var renderedWidget = new SuggestPageWidget().renderedWidget;

      expect(renderedWidget.suggestedPage.getText()).toBe(name);

      // remove widget
      var suggestPageWidget = new SuggestPageWidget(widgetSlot.getWidget(0));
      suggestPageWidget.hover();
      suggestPageWidget.removeButton.click().then(function () {
        components.modals.confirm.confirmButton.click();
        navigation.viewEditOptions.saveButton.click();
        expect(widgetSlot.allWidgets.count()).toBe(0);
        expect(widgetSlot.addButton.isPresent()).toBe(false);
      });
    });
  });

})();
