(function () {
  'use strict';

  function Search() {
    var api = this;

    var filterBox = $('.fb-filter-inline');
    api.filter = {
      all: filterBox.$('li[text-key="MODULE.SEARCH.FILTER.TYPE.ALL"]'),
      page: filterBox.$('li[text="Page"]'),
      wikiArticle: filterBox.$('li[text="Wiki Article"]'),
      ever: filterBox.$('li[text-key="MODULE.SEARCH.FILTER.MODIFIED.EVER"]'),
      lastYear: filterBox.$('li[text-key="MODULE.SEARCH.FILTER.MODIFIED.LAST_YEAR"]'),
      lastMonth: filterBox.$('li[text-key="MODULE.SEARCH.FILTER.MODIFIED.LAST_MONTH"]'),
      lastWeek: filterBox.$('li[text-key="MODULE.SEARCH.FILTER.MODIFIED.LAST_WEEK"]')
    };

    api.searchresults = {
      items: $$('.search-result'),
      itemByIndex: function (index) {
        return api.searchresults.items.then(function (array) {
          return array[index];
        });
      },
      itemByName: function (name) {
        return element(by.cssContainingText('.search-result-name', name));
      }
    };
  }

  module.exports = Search;
})();
