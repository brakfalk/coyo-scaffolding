(function () {
  'use strict';

  var testhelper = require('./testhelper');
  var NavigationPage = require('./desktop/navigation.page.js');
  var navigation = new NavigationPage();

  var loginPage = {
    get: function () {
      this.logout(); // prevent redirect to main state if already logged in
      browser.get('/f/login');
      testhelper.disableAnimations();
    },
    loginDefaultUser: function (doNotHideTour) {
      this.login('ian.bold@coyo4.com', 'demo', doNotHideTour);
    },
    loginDefaultAdminUser: function (doNotHideTour) {
      this.loginDefaultUser(doNotHideTour);
    },
    login: function (username, password, doNotHideTour) {
      this.get();
      loginPage.selectDefaultBackend();
      this.usernameInput.isPresent().then(function (isPresent) {
        if (isPresent) {
          loginPage.usernameInput.clear(); // clear field before enter new username
          loginPage.usernameInput.sendKeys(username);
          loginPage.passwordInput.sendKeys(password);
          loginPage.loginButton.click();
          // redirect to /home if user does not already lands there
          browser.getCurrentUrl().then(function (actualUrl) {
            if (actualUrl.endsWith('/search')) {
              browser.get('/home');
            }
          });
          if (!doNotHideTour) {
            testhelper.cancelTour();
          }
        }
      });
    },
    logout: function () {
      browser.get('/search'); // load search page to be in protractor context otherwise '$ is not defined'
      testhelper.cancelTour();
      navigation.profile.isPresent().then(function (isPresent) {
        if (isPresent) {
          navigation.logout(); // prevent redirect to main state if already logged in
        }
      });
    },
    logoutMessageHeader: $('h2[translate="MODULE.LOGIN.LOGOUT.SUCCESS.HEADLINE"]'),
    selectDefaultBackend: function () {
      loginPage.backendSelector.urlInput.isPresent().then(function (isPresent) {
        if (isPresent) {
          loginPage.backendSelector.urlInput.sendKeys('http://localhost:8080');
          loginPage.backendSelector.continueButton.click();
        }
      });
    },
    isLoggedin: function () {
      return $('.navbar').isPresent().then(function (isLoggedIn) {
        return isLoggedIn;
      });
    },
    backendSelector: {
      urlInput: element(by.model('$ctrl.url')),
      continueButton: $('span[translate="MODULE.LOGIN.CONFIGURE.SUBMIT"]')
    },
    usernameInput: element(by.model('$ctrl.user.username')),
    passwordInput: element(by.model('$ctrl.user.password')),
    loginButton: $('button[type="submit"]'),
    logoutButton: $('a[ng-click="$ctrl.logout()"]'),
    errorMessage: $('#login-error')
  };

  module.exports = loginPage;

})();
