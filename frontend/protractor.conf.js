(function () {
  'use strict';
  /* global require, exports, process, browser, jasmine, Promise */

  var conf = require('./gulp/conf');
  var gutil = require('gulp-util');
  var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
  var webdriver = require('selenium-webdriver');

  var screenshotReporter = new HtmlScreenshotReporter({
    dest: 'test-results/screenshots',
    filename: 'protractor.html',
    ignoreSkippedSpecs: true,
    showSummary: true,
    showQuickLinks: true,
    captureOnlyFailedSpecs: true
  });

  // Default local configuration
  exports.config = {
    directConnect: true,

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
      'browserName': 'chrome',
      'chromeOptions': {
        args: ['--window-size=1400,1024']
      }
    },

    baseUrl: 'http://localhost:3000',

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
      showColors: true,
      defaultTimeoutInterval: 120000
    },

    allScriptsTimeout: 30000,

    beforeLaunch: function () {
      return new Promise(function (resolve) {
        screenshotReporter.beforeLaunch(resolve);
      });
    },

    onPrepare: function () {
      require('./src/test-e2e/jasmine.matchers');
      var reporters = require('jasmine-reporters');
      var testhelper = require('./src/test-e2e/testhelper');
      var login = require('./src/test-e2e/login.page');
      jasmine.getEnv().addReporter(new reporters.JUnitXmlReporter({
        consolidateAll: true,
        savePath: 'test-results',
        filePrefix: 'TESTS-protractor-desktop'
      }));
      jasmine.getEnv().addReporter(new reporters.TerminalReporter({verbosity: 3}));
      jasmine.getEnv().addReporter(screenshotReporter);


      afterEach(function () {
        testhelper.ignorePageLeaveAlert();
      });

      afterAll(function () {
        testhelper.ignorePageLeaveAlert();
        testhelper.disableAnimations();
        // desktop logout
        login.isLoggedin().then(function (isLoggedIn) {
          if (isLoggedIn) {
            login.logout();
          }
        });
      });

      // workaround: chromedriver can no longer click on element outside viewport so scroll to it
      var handler = webdriver.WebElement.prototype.click;
      webdriver.WebElement.prototype.click = function () {
        browser.executeScript('arguments[0].scrollIntoView()', this);
        return handler.apply(this, arguments);
      };

    },

    afterLaunch: function (exitCode) {
      return new Promise(function (resolve) {
        screenshotReporter.afterLaunch(resolve.bind(this, exitCode));
      });
    }
  };

  // Environment variables override
  if (process.env.COYO_E2E_HOST) {
    gutil.log('Setting selenium host to: ', process.env.COYO_E2E_HOST);
    exports.config.seleniumAddress = process.env.COYO_E2E_HOST;
    exports.config.directConnect = false;
  }
  if (process.env.COYO_E2E_BASEURL) {
    gutil.log('Setting e2e base URL to: ', process.env.COYO_E2E_BASEURL);
    exports.config.baseUrl = process.env.COYO_E2E_BASEURL;
  }

  if (process.env.bamboo_COYO_E2E_HOST) {
    gutil.log('Setting selenium host to: ', process.env.bamboo_COYO_E2E_HOST);
    exports.config.seleniumAddress = process.env.bamboo_COYO_E2E_HOST;
    exports.config.directConnect = false;
  }
  if (process.env.bamboo_COYO_E2E_BASEURL) {
    gutil.log('Setting e2e base URL to: ', process.env.bamboo_COYO_E2E_BASEURL);
    exports.config.baseUrl = process.env.bamboo_COYO_E2E_BASEURL;
  }
})();
